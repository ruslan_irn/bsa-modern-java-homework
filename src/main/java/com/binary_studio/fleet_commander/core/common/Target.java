package com.binary_studio.fleet_commander.core.common;

public class Target implements NamedEntity {

	private String name;

	public Target(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
