package com.binary_studio.fleet_commander.core.common;

public class Attacker implements NamedEntity {

	private String name;

	public Attacker(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
