package com.binary_studio.fleet_commander.core.common;

public class Weapon implements NamedEntity {

	private String name;

	public Weapon(String name) {
		this.name = name;
	}

	@Override
	public String getName() {

		return this.name;
	}

}
