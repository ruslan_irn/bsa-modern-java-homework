package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public DefenciveSubsystemImpl(String name, PositiveInteger impactReduction, PositiveInteger shieldRegen,
			PositiveInteger hullRegen, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.impactReduction = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name.strip().length() == 0) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name, impactReductionPercent, shieldRegeneration, hullRegeneration,
				capacitorConsumption, powergridConsumption);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		PositiveInteger reducedDamage;
		if (this.impactReduction.value() < 95) {
			reducedDamage = PositiveInteger.of((int) Math.ceil((double) incomingDamage.damage.value()
					- ((double) incomingDamage.damage.value() / 100) * this.impactReduction.value()));
		}
		else {
			reducedDamage = PositiveInteger.of((int) Math.ceil(incomingDamage.damage.value() * 0.05));
		}
		return new AttackAction(reducedDamage, incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
