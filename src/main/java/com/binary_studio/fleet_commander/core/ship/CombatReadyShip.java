package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.Attacker;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.common.Target;
import com.binary_studio.fleet_commander.core.common.Weapon;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private DockedShip dockedShip;

	private PositiveInteger initialCapacitorCharge;

	private PositiveInteger initialHull;

	private PositiveInteger initialShield;

	public CombatReadyShip(DockedShip dockedShip) {
		this.dockedShip = dockedShip;
		this.initialCapacitorCharge = PositiveInteger.of(dockedShip.getCapacitor().value());
		this.initialHull = PositiveInteger.of(dockedShip.getHullHP().value());
		this.initialShield = PositiveInteger.of(dockedShip.getShieldHP().value());
	}

	@Override
	public void endTurn() {
		this.dockedShip.setCapacitor(this.initialCapacitorCharge.value());
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.dockedShip.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.dockedShip.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.dockedShip.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		PositiveInteger damage = this.dockedShip.getAttackSubsystem().attack(target);
		if (this.dockedShip.getCapacitor().value() < this.dockedShip.getAttackSubsystem().getCapacitorConsumption()
				.value()) {
			return Optional.empty();
		}
		this.dockedShip
				.setCapacitor(
						PositiveInteger
								.of(this.dockedShip.getCapacitor().value()
										- this.dockedShip.getAttackSubsystem().getCapacitorConsumption().value())
								.value());
		return Optional.of(new AttackAction(damage, new Attacker(this.dockedShip.getName()),
				new Target(target.getName()), new Weapon(this.dockedShip.getAttackSubsystem().getName())));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction midifiedAttackAction = this.dockedShip.getDefenciveSubsystem().reduceDamage(attack);
		PositiveInteger damageToApply = PositiveInteger.of(midifiedAttackAction.damage.value());
		if (this.dockedShip.getShieldHP().value() > 0 && this.dockedShip.getShieldHP().value() > 0) {
			if (damageToApply.value() > this.dockedShip.getShieldHP().value()) {
				damageToApply = PositiveInteger.of(damageToApply.value() - this.dockedShip.getShieldHP().value());
				this.dockedShip.setShieldHP(0);
			}
			else {
				this.dockedShip.setShieldHP(this.dockedShip.getShieldHP().value() - damageToApply.value());
				damageToApply = PositiveInteger.of(0);
			}
		}
		if (damageToApply.value() > 0 && this.dockedShip.getHullHP().value() > 0) {
			if (damageToApply.value() > this.dockedShip.getHullHP().value()) {
				this.dockedShip.setHullHP(0);
			}
			else {
				this.dockedShip.setHullHP(this.dockedShip.getHullHP().value() - damageToApply.value());
				damageToApply = PositiveInteger.of(0);
			}
		}
		if (this.dockedShip.getShieldHP().value() == 0 && this.dockedShip.getHullHP().value() == 0) {
			return new AttackResult.Destroyed();
		}
		return new AttackResult.DamageRecived(attack.weapon, midifiedAttackAction.damage, new Target(getName()));
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.dockedShip.getCapacitor().value() < this.dockedShip.getDefenciveSubsystem().getCapacitorConsumption()
				.value()) {
			return Optional.empty();
		}
		this.dockedShip.setCapacitor(this.dockedShip.getCapacitor().value()
				- this.dockedShip.getDefenciveSubsystem().getCapacitorConsumption().value());
		PositiveInteger appliedHullValue = PositiveInteger.of(0);
		PositiveInteger appliedShieldValue = PositiveInteger.of(0);
		RegenerateAction ra = this.dockedShip.getDefenciveSubsystem().regenerate();

		if (this.initialHull.value() > this.dockedShip.getHullHP().value()) {
			if ((this.initialHull.value() - this.dockedShip.getHullHP().value()) > ra.getHullHPRegenerated().value()) {
				appliedHullValue = PositiveInteger.of(ra.getHullHPRegenerated().value());
				this.dockedShip.setHullHP(this.dockedShip.getHullHP().value() + appliedHullValue.value());
			}
			else {
				this.dockedShip.setHullHP(this.initialHull.value());
			}
		}
		if (this.initialShield.value() > this.dockedShip.getShieldHP().value()) {
			if ((this.initialShield.value() - this.dockedShip.getShieldHP().value()) > ra.getShieldHPRegenerated()
					.value()) {
				appliedShieldValue = PositiveInteger.of(ra.getShieldHPRegenerated().value());
				this.dockedShip.setShieldHP(this.dockedShip.getShieldHP().value() + appliedShieldValue.value());
			}
			else {
				this.dockedShip.setShieldHP(this.initialShield.value());
			}
		}
		return Optional.of(new RegenerateAction(appliedShieldValue, appliedHullValue));
	}

}
