package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private DefenciveSubsystem defenciveSubsystem;

	private AttackSubsystem attackSubsystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, powergridOutput, speed,
				size);
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem.getPowerGridConsumption().value().compareTo(this.pg.value()) >= 0) {
			Integer neededMorePower = subsystem.getPowerGridConsumption().value() - this.pg.value();
			throw new InsufficientPowergridException(neededMorePower);
		}
		if (this.defenciveSubsystem != null && subsystem.getPowerGridConsumption().value()
				.compareTo(this.pg.value() - this.defenciveSubsystem.getPowerGridConsumption().value()) > 0) {
			Integer neededMorePower = subsystem.getPowerGridConsumption().value()
					+ this.defenciveSubsystem.getPowerGridConsumption().value() - this.pg.value();
			throw new InsufficientPowergridException(neededMorePower);
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			if (subsystem.getPowerGridConsumption().value().compareTo(this.pg.value()) > 0) {
				Integer neededMorePower = subsystem.getPowerGridConsumption().value() - this.pg.value();
				throw new InsufficientPowergridException(neededMorePower);
			}
			if (this.attackSubsystem != null && subsystem.getPowerGridConsumption().value()
					.compareTo(this.pg.value() - this.attackSubsystem.getPowerGridConsumption().value()) > 0) {
				Integer neededMorePower = subsystem.getPowerGridConsumption().value()
						+ this.attackSubsystem.getPowerGridConsumption().value() - this.pg.value();
				throw new InsufficientPowergridException(neededMorePower);
			}
		}
		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem == null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		return new CombatReadyShip(this);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public void setShieldHP(Integer shieldHP) {
		this.shieldHP = PositiveInteger.of(shieldHP);
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public void setHullHP(Integer hullHP) {
		this.hullHP = PositiveInteger.of(hullHP);
	}

	public PositiveInteger getCapacitor() {
		return this.capacitor;
	}

	public void setCapacitor(Integer capacitor) {
		this.capacitor = PositiveInteger.of(capacitor);
	}

	public PositiveInteger getCapacitorRegeneration() {
		return this.capacitorRegeneration;
	}

	public void setCapacitorRegeneration(Integer capacitorRegeneration) {
		this.capacitorRegeneration = PositiveInteger.of(capacitorRegeneration);
	}

	public PositiveInteger getPg() {
		return this.pg;
	}

	public void setPg(Integer pg) {
		this.pg = PositiveInteger.of(pg);
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public void setSpeed(Integer speed) {
		this.speed = PositiveInteger.of(speed);
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public void setSize(Integer size) {
		this.size = PositiveInteger.of(size);
	}

}
