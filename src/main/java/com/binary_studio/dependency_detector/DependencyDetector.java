package com.binary_studio.dependency_detector;

import java.util.List;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		if ( libraries == null ) { return false; }
		if ( libraries.dependencies == null ||libraries.libraries == null ) {return false;}
		int i;
		for (i = 0; i < libraries.libraries.size(); i++) {
			if (isCircular(libraries.dependencies, libraries.libraries.get(i))) {
				return false;
			}
		}
		return true;
	}

	public static boolean isCircular(List<String[]> dependencies, String searchTerm) {
		String dependency = searchTerm;
		while (dependency != null) {
			dependency = getDependency(dependencies, dependency);
			if (dependency != null && dependency.equals(searchTerm)) {
				return true;
			}
		}
		return false;
	}

	public static String getDependency(List<String[]> dependencies, String searchTerm) {
		String dependency = null;
		String[] depPair;
		int i;
		for (i = 0; i < dependencies.size(); i++) {
			depPair = dependencies.get(i);
			if (depPair[0].equals(searchTerm)) {
				return depPair[1];
			}
		}
		return dependency;
	}

}
