package com.binary_studio.academy_coin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		if ( prices == null ) { return 0; }
		int i;
		boolean hasMax = false;
		boolean hasMin = false;
		List<PairOfMinMax> pairs = new ArrayList<>();
		int[] arrayOfPrices = prices.filter(el -> Objects.nonNull(el)).mapToInt(e -> e).toArray();
		for ( int k : arrayOfPrices ) {
			System.out.println(k);
		}
		if ( arrayOfPrices.length == 0 ) { return 0; }
		int min = arrayOfPrices[0];
		int max = -1;
		for (i = 0; i < arrayOfPrices.length; i++) {
			if (max == -1 && arrayOfPrices[i] <= min) {
				System.out.println("here");
				min = arrayOfPrices[i];
				if (i + 1 < arrayOfPrices.length && arrayOfPrices[i + 1] > min) {
					hasMin = true;
					max = arrayOfPrices[i];
				}
			}
			System.out.println("first sout min=" + min + " max=" + max);
			if (hasMin && arrayOfPrices[i] > max) {
				max = arrayOfPrices[i];
				if (i + 1 < arrayOfPrices.length && arrayOfPrices[i + 1] < max) {
					hasMax = true;
				}
				if (i == arrayOfPrices.length - 1) {
					hasMax = true;
				}
			}
			if (min == arrayOfPrices[0] && i == arrayOfPrices.length - 1) {
				max = arrayOfPrices[i];
				hasMax = true;
				hasMin = true;
			}
			System.out.println("min=" + min + " max=" + max);
			if (hasMin && hasMax) {
				pairs.add(new PairOfMinMax(min, max));
				max = -1;
				min = arrayOfPrices[i];
				hasMax = false;
				hasMin = false;
			}
		}
		pairs.stream().forEach(elm -> System.out.println(elm.toString()));
		return calculateSum(pairs);
	}

	public static int calculateSum(List<PairOfMinMax> pairsOfMinMaxs) {
		
		if (pairsOfMinMaxs.isEmpty()) {
			return 0;
		}
		int sum = 0;
		for (PairOfMinMax pair : pairsOfMinMaxs) {
			sum = sum + (pair.getMax() - pair.getMin());
		}
		return sum;
	}

}

class PairOfMinMax {

	private int min;

	private int max;

	PairOfMinMax(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return this.min;
	}

	public int getMax() {
		return this.max;
	}

	@Override
	public String toString() {
		return "PairOfMinMax [min=" + this.min + ", max=" + this.max + "]";
	}

}
